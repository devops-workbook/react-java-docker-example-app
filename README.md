# React-Springboot-App

Тренировочный проект для мультистейдж сборки frontend и backend компонентов приложения в Docker и настройки CI

## Технологии

- ReactJs
- Spring boot
- Docker
- Gitlab CI

## Локальный запуск

#### Run the following commands
```
docker-compose up
```

